import adapter from '@sveltejs/adapter-node'
import preprocess from 'svelte-preprocess'
import path from 'path'

/** @type {import('@sveltejs/kit').Config} */
const config = {
    kit: {
        adapter: adapter(),
        vite: {
            resolve: {
                alias: {
                    $components: path.resolve('./src/components/'),
                    $models: path.resolve('./src/models/'),
                    $utils: path.resolve('./src/utils/')
                }
            }
        }
    },

    preprocess: preprocess({ sourceMap: true, postcss: true })
}

export default config
