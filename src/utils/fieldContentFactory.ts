import DistrictCouncil from '$models/Buildings/DistrictCouncil'
import EleriumAccelerator from '$models/Buildings/Resources/Production/EleriumAccelerator'
import GraphiteMine from '$models/Buildings/Resources/Production/GraphiteMine'
import GraphiteStorage from '$models/Buildings/Resources/Storage/GraphiteStorage'
import HydroponicFields from '$models/Buildings/Resources/Production/HydroponicFields'
import MetalMine from '$models/Buildings/Resources/Production/MetalMine'
import EleriumStorage from '$models/Buildings/Resources/Storage/EleriumStorage'
import MetalStorage from '$models/Buildings/Resources/Storage/MetalStorage'
import PasteStorage from '$models/Buildings/Resources/Storage/PasteStorage'
import Labs from '$models/Buildings/Resources/Production/Labs'
import Starshipyard from '$models/Buildings/Starshipyard'
import { FertileSoil, GraphiteDeposit, MetalDeposit, MeteoriteImpact } from '$models/Natural'
import type DistrictField from '$models/DistrictField'
import type { FieldResource } from '$utils/API/District.type'
import type District from '$models/District'

export default function (fieldResource: FieldResource, district: District): DistrictField {
    return new { ...buildings, ...naturals }[fieldResource.value](
        fieldResource.x,
        fieldResource.y,
        district,
        fieldResource.level
    )
}

export const buildings = {
    DistrictCouncil,
    Labs,
    EleriumAccelerator,
    GraphiteMine,
    HydroponicFields,
    MetalMine,
    EleriumStorage,
    GraphiteStorage,
    MetalStorage,
    PasteStorage,
    Starshipyard
}

const naturals = {
    FertileSoil,
    GraphiteDeposit,
    MetalDeposit,
    MeteoriteImpact
}
