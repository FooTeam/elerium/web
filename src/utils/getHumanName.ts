export default function (name: string): string {
    return name
        .split(/([A-Z][a-z]+)/)
        .filter(Boolean)
        .join(' ')
}
