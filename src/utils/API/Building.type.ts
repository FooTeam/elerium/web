export type ProductionDetails = {
    base: number
    naturals: {
        number: number
        boost: number // boost percentage per natural
    }
    researchBoost: number // percentage
    total: number // = base * (1 + researchBoost + naturals.boost * naturals.number)
}

export type StorageDetails = {
    base: number
    researchBoost: number // percentage
    total: number // = base * (1 + researchBoost)
}

export type Building = {
    name: string
    level: number
    up: {
        neededMetal: number
        neededGraphite: number
    }
}

export type StorageBuilding = Building & {
    storage: StorageDetails
    nextLevelStorage: number
}

export type ProductionBuilding = Building & {
    production: ProductionDetails
    nextLevelProduction: number
}

export type Starshipyard = Building & {
    unitStorage: number
    nextLevelStorage: number
}
