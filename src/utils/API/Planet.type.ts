enum color {
    red = 'red',
    yellow = 'yellow',
    orange = 'orange',
    violet = 'violet'
}

export type District = {
    id: number
    x: number
    y: number
    type: 'district'
    name: string
    owner: string
    faction: string
}

export type Stargate = {
    id: number
    x: number
    y: number
    type: 'stargate'
    color: color
    owner?: string
    units?: Record<string, number>
    faction?: string
}

export type PlanetField = District | Stargate

export type PlanetCollection = {
    [x: number]: {
        [y: number]: [PlanetField]
    }
}
