export type ResearchResource = {
    name: string
    needed_science: number
    needed_elerium: number
    unlocked: boolean
    unlocks: ResearchResource[]
}
