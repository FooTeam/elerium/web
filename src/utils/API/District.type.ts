export type FieldResource = {
    x: number
    y: number
    value: string
    level: number
}

export type UnitNeededResources = {
    metal: number
    elerium: number
    paste: number
}

export type UnitResource = {
    type: string
    quantity: number
    speed: number
    defense: number
    attack: number
    build: UnitNeededResources
}

export type DistrictResource = {
    id: number
    name: string
    created_at: string
    fields: FieldResource[]
    unitStorage: number
}

export type DistrictResourceCollection = {
    data: DistrictResource[]
}
