import type { Readable, Writable } from 'svelte/store'
import { derived, get as storeGet, writable } from 'svelte/store'
import type Building from '$models/Building'
import type { neededResources } from '$models/Building'
import type DistrictCouncil from '$models/Buildings/DistrictCouncil'
import type DistrictField from '$models/DistrictField'
import type { Natural } from '$models/Natural'
import Unit from '$models/Unit'
import { get, post } from '$lib/fetch'
import type { Resources } from '$lib/resource'
import {
    BuildingMustBeResearchedError,
    DistrictLevelTooLowError,
    NotEnoughResourcesError,
    pushError
} from '$lib/errors'
import type { DistrictResource, FieldResource, UnitResource } from '$utils/API/District.type'
import fieldContentFactory from '$utils/fieldContentFactory'
import StarshipyardsFullError from '$lib/errors/Errors/StarshipyardsFullError'

export default class District {
    id: number
    name: string
    created_at: string
    grid: Writable<{ [p: number]: { [p: number]: Building | Natural } }>
    buildings: Readable<Building[]>
    unitStorage: Writable<number>
    unitNumber: Readable<number>
    units: Writable<Unit[]>
    private resources: Writable<Resources>

    constructor({ id, created_at, name, fields, unitStorage }: DistrictResource) {
        this.id = id
        this.name = name
        this.created_at = created_at
        this.unitStorage = writable(unitStorage)
        this.addDistrictGrid(fields)
        this.deriveBuildingList()
        this.addUnitStore()
    }

    private getDistrictCouncil(): DistrictCouncil {
        return storeGet(this.grid)[5][7] as DistrictCouncil
    }

    getDistrictLevel(): number {
        return this.getDistrictCouncil().level
    }

    async setResourcesStore(): Promise<void> {
        this.resources = writable((await get(`api/districts/${this.id}/resources`)).res)

        this.updateResource()
    }

    getResources(): {
        store: Writable<Resources>
        resources: Resources
    } {
        const store = this.resources
        return { store, resources: storeGet(store) }
    }

    async addUnit(
        type: string,
        quantity: number
    ): Promise<{ res: UnitResource; req: Response }> | null {
        let unit: Unit
        for (const u of storeGet(this.units)) {
            if (u.type === type) {
                unit = u
                break
            }
        }

        const neededResources = {} as neededResources
        for (const name in unit.build) {
            neededResources[name] = unit.build[name] * quantity
        }

        if (!this.checkNeededResources(neededResources)) {
            const resources = this.getResources().resources
            pushError(
                new NotEnoughResourcesError(
                    unit.type,
                    {
                        name: 'metal',
                        have: resources.metal.value,
                        need: neededResources.metal
                    },
                    {
                        name: 'elerium',
                        have: resources.elerium.value,
                        need: neededResources.elerium
                    },
                    {
                        name: 'paste',
                        have: resources.paste.value,
                        need: neededResources.paste
                    }
                )
            )

            return null
        }

        if (storeGet(this.unitStorage) < storeGet(this.unitNumber) + quantity) {
            pushError(new StarshipyardsFullError())
            return null
        }

        this.consumeResource(neededResources)

        return await post(`api/districts/${this.id}/newUnit`, {
            type: type,
            quantity: quantity
        })
    }

    async upBuilding(building: Building): Promise<Building> {
        if (
            building.constructor.name !== 'DistrictCouncil' &&
            building.level + 1 > this.getDistrictLevel()
        ) {
            pushError(new DistrictLevelTooLowError(building.name()))
            return building
        }

        const neededResources = building.getNeededResources()
        if (!this.checkNeededResources(neededResources)) {
            const resources = this.getResources().resources
            pushError(
                new NotEnoughResourcesError(
                    building.name(),
                    {
                        name: 'graphite',
                        have: resources.graphite.value,
                        need: neededResources.graphite
                    },
                    {
                        name: 'metal',
                        have: resources.metal.value,
                        need: neededResources.metal
                    }
                )
            )
            return building
        }

        if (building.level === 0) {
            await post(`api/districts/${this.id}/newBuilding`, {
                value: building.constructor.name,
                x: building.x,
                y: building.y
            }).catch(error => {
                if (error.neededResearch) {
                    pushError(new BuildingMustBeResearchedError(building.name()))
                }
                throw error
            })
        } else {
            await post(`api/districts/${this.id}/upgrade`, {
                x: building.x,
                y: building.y
            })
        }

        this.consumeResource(neededResources)
        building.upgrade()

        this.grid.update(grid => {
            grid[building.y][building.x] = building
            return grid
        })

        return building
    }

    private addDistrictGrid(fields: FieldResource[]) {
        // start with an empty grid
        const grid = {
            ...new Array(11).fill(null).map(() => {
                return { ...new Array(15).fill(null) }
            })
        }

        for (const field of fields) {
            grid[field.y][field.x] = fieldContentFactory(field, this)
        }
        this.grid = writable(grid)
    }

    private deriveBuildingList(): void {
        this.buildings = derived(
            this.grid,
            $grid =>
                Object.values($grid).reduce((accumulator: any[], line) => {
                    return accumulator.concat(
                        Object.values(line).filter(
                            (field: DistrictField) => field && field.isBuilding()
                        )
                    )
                }, []),
            []
        )
    }

    private updateResource(): void {
        setInterval(() => {
            const resourcesData = storeGet(this.resources)

            const array = ['elerium', 'graphite', 'metal', 'paste']
            array.forEach(resourceName => {
                const newValue =
                    resourcesData[resourceName].value + resourcesData[resourceName].production
                resourcesData[resourceName].value =
                    newValue > resourcesData[resourceName].max
                        ? resourcesData[resourceName].max
                        : newValue
                this.resources.set(resourcesData)
            })
        }, 60000)
    }

    private checkNeededResources(neededResources: neededResources): boolean {
        const { resources } = this.getResources()

        return ['metal', 'graphite', 'elerium', 'paste'].every(name => {
            if (undefined === neededResources[name]) return true

            return resources[name].value >= neededResources[name]
        })
    }

    private consumeResource(neededResources: neededResources): void {
        const { store, resources } = this.getResources()
        ;['metal', 'graphite', 'elerium', 'paste'].forEach(name => {
            if (undefined === neededResources[name]) return

            return (resources[name].value -= neededResources[name])
        })
        store.set(resources)
    }

    private addUnitStore(): void {
        this.units = writable([], set => {
            get(`api/districts/${this.id}/units`).then(({ res }: { res: UnitResource[] }) => {
                set(res.map(unit => Unit.makeFromApiResource(unit)))
            })
        }) as Writable<Unit[]>
        this.unitNumber = derived(this.units, $units =>
            $units.reduce((acc, curr) => acc + curr.quantity, 0)
        )
    }
}
