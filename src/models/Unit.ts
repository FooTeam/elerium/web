import type { UnitResource, UnitNeededResources } from '$utils/API/District.type'

export default class Unit {
    constructor(
        public attack: number,
        public defense: number,
        public speed: number,
        public type: string,
        public quantity: number,
        public build: UnitNeededResources
    ) {}

    static makeFromApiResource = (unit: UnitResource): Unit => {
        return new Unit(unit.attack, unit.defense, unit.speed, unit.type, unit.quantity, unit.build)
    }
}
