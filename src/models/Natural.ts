import DistrictFieldContent, {
    DistrictFieldColor,
    DistrictFieldGradientType
} from '$models/DistrictField'
import type District from '$models/District'

export abstract class Natural extends DistrictFieldContent {}

export class FertileSoil extends Natural {
    color: DistrictFieldColor
    description = `These fertile soils will provide you with incredible 
        nutrient paste creation bonuses. It will be better and more nutritious.`

    constructor(x: number, y: number, district: District) {
        super(x, y, district)
        this.color = {
            type: DistrictFieldGradientType.linGrad,
            colors: Array(21)
                .fill(null)
                .map((_, index) => {
                    if (index % 2 === 0) {
                        return {
                            offset: (1 / 21) * index,
                            color: '#713F12'
                        }
                    }
                    return { offset: (1 / 21) * index, color: '#4D7C0F' }
                })
        }
    }
}

export class GraphiteDeposit extends Natural {
    color: DistrictFieldColor
    description = `These graphite plots scattered over your district are 
        pretty well timed. Take the opportunity to collect as much as you can.`

    constructor(x: number, y: number, district: District) {
        super(x, y, district)
        this.color = {
            type: DistrictFieldGradientType.linGrad,
            colors: [
                { offset: 0.4, color: '#44403C' },
                { offset: 0.6, color: '#292524' },
                { offset: 1, color: '#B45309' }
            ]
        }
    }
}

export class MetalDeposit extends Natural {
    color: DistrictFieldColor
    description = `It looks like metal shards are on its floors. You 
        guys are really lucky, so go and pick it up.`

    constructor(x: number, y: number, district: District) {
        super(x, y, district)
        this.color = {
            type: DistrictFieldGradientType.linGrad,
            colors: [
                { offset: 0, color: '#D1D5DB' },
                { offset: 0.4, color: '#9CA3AF' },
                { offset: 1, color: '#D1D5DB' }
            ]
        }
    }
}

export class MeteoriteImpact extends Natural {
    color: DistrictFieldColor
    description = `Unbelievable! A meteorite impact! Place your elerium
        accelerator in the neighborhood and you will be pleasantly surprised!`

    constructor(x: number, y: number, district: District) {
        super(x, y, district)
        this.color = {
            type: DistrictFieldGradientType.radGrad,
            colors: [
                { offset: 0.1, color: '#020538' },
                { offset: 0.5, color: '#191f69' },
                { offset: 1, color: '#020644' }
            ]
        }
    }
}
