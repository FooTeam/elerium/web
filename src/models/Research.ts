import { post } from '$lib/fetch'
import { NeededScienceError, NeededEleriumError, pushError } from '$lib/errors'
import { LockedResearchError } from '$lib/errors/Errors/LockedResearch'
import getHumanName from '$utils/getHumanName'
import type { ResearchResource } from '$utils/API/Research.type'

export default class Research {
    id: number = Math.floor(Math.random() * 1000)
    ref: HTMLDivElement
    name: string
    needed_science: number
    needed_elerium: number
    unlocked: boolean
    unlocks: Research[]
    parent: Research

    constructor(
        { name, needed_science, needed_elerium, unlocks, unlocked }: ResearchResource = {
            name: '',
            needed_science: 1,
            needed_elerium: 1,
            unlocks: [],
            unlocked: false
        },
        parent: Research = null
    ) {
        this.name = name
        this.needed_science = needed_science
        this.needed_elerium = needed_elerium
        this.unlocked = unlocked
        this.unlocks = unlocks
            ? unlocks.map((unlock: ResearchResource) => {
                  return new Research(unlock, this)
              })
            : []
        this.parent = parent
    }

    public async unlock(): Promise<void> {
        if (!this.unlocked) {
            await post('api/researches/start', { name: this.name })
                .then(() => (this.unlocked = true))
                .catch(error => {
                    if (error.errors.includes('locked')) {
                        pushError(new LockedResearchError(getHumanName(error.neededResearch)))
                    }
                    if (error.errors.includes('science')) {
                        pushError(
                            new NeededScienceError(
                                this.name,
                                error.messages.find((message: string) =>
                                    message.includes('science')
                                )
                            )
                        )
                    }
                    if (error.errors.includes('science')) {
                        pushError(
                            new NeededEleriumError(
                                error.messages.find((message: string) =>
                                    message.includes('elerium')
                                )
                            )
                        )
                    }
                })
        }
    }
}
