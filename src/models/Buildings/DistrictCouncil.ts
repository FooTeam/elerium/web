import Building from '$models/Building'

export default class DistrictCouncil extends Building {
    color = '#0EA5E9'
    description = `It's a bit like a city hall. Or maybe a capital.
        Take care of it, it's better for the conquest of the universe.`
}
