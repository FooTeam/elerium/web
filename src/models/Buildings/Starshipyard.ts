import Building from '$models/Building'
import type { Starshipyard as StarshipyardApiType } from '$utils/API/Building.type'

export default class Starshipyard extends Building {
    color = '#0EA5E9'
    description = `Without shipyards, no troops. Without troops, no stargates. 
        Without stargates, no victory. Without victory... You got it.`
    unitStorage = 0
    nextLevelStorage: number

    setUnitStorage(res: StarshipyardApiType): void {
        this.unitStorage = res.unitStorage
        this.nextLevelStorage = res.nextLevelStorage
    }

    upgrade(): void {
        const diff = this.nextLevelStorage - this.unitStorage
        super.upgrade()
        this.district.unitStorage.update(max => max + diff)
    }
}
