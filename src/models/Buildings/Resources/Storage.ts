import Building from '$models/Building'
import type { StorageBuilding as ApiType, StorageDetails } from '$utils/API/Building.type'

export default abstract class Storage extends Building {
    abstract resourceName: string
    nextLevelStorage: number
    storage: StorageDetails = null

    // Update the capacity in the store
    upgrade(): void {
        const { store, resources } = this.district.getResources()
        const diff = this.nextLevelStorage - this.storage.total
        super.upgrade()
        resources[this.resourceName].max += diff
        store.set(resources)
    }

    protected setApiData(res: ApiType): void {
        super.setApiData.call(this, res)

        this.nextLevelStorage = res.nextLevelStorage
        this.storage = res.storage
    }
}
