import Building from '$models/Building'
import type { ProductionBuilding as ApiType, ProductionDetails } from '$utils/API/Building.type'

export default abstract class Production extends Building {
    abstract resourceName: string
    nextLevelProduction: number
    production: ProductionDetails = null

    upgrade(): void {
        const { store, resources } = this.district.getResources()
        const diff = this.nextLevelProduction - this.production.total
        super.upgrade()
        resources[this.resourceName].production += diff
        store.set(resources)
    }

    protected setApiData(res: ApiType): void {
        super.setApiData.call(this, res)

        this.nextLevelProduction = res.nextLevelProduction
        this.production = res.production
    }
}
