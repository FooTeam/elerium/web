import Storage from '../Storage'

export default class EleriumStorage extends Storage {
    resourceName = 'elerium'
    color = '#101555'
    description = `Because of its instability, elerium must be
        stored under high pressure and at low temperature.`
}
