import Storage from '../Storage'

export default class GraphiteStorage extends Storage {
    resourceName = 'graphite'
    color = '#44403C'
    description = `Because it is brittle graphite can be stored easily,
        preferred in dry places, it would be a shame to see it rot.`
}
