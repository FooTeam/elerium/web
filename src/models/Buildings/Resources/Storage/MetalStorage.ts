import Storage from '../Storage'

export default class MetalStorage extends Storage {
    resourceName = 'metal'
    color = '#9CA3AF'
    description = `Nothing better to store metal than a large double-locked
        hangar. Don't let humidity settle in, you could be disappointed.`
}
