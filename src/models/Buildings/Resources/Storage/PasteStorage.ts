import Storage from '../Storage'

export default class PasteStorage extends Storage {
    resourceName = 'paste'
    color = '#4D7C0F'
    description = `The storage of the nutritious paste requires
        a dry and cool place to keep their unique flavor.`
}
