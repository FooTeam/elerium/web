import Production from '../Production'

export default class GraphiteMine extends Production {
    resourceName = 'graphite'
    color = '#44403C'
    description = `Graphite is a brittle material but is highly valued for
        its quality. It will be of great help in your exploration of the galaxy.`
}
