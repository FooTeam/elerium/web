import Production from '../Production'

export default class Labs extends Production {
    resourceName = 'science'
    color = '#0EA5E9'
    description = `Labs allow you to increase your
        research points to unlock many essential upgrades.`
}
