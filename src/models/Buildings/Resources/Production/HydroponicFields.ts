import Production from '../Production'

export default class HydroponicFields extends Production {
    resourceName = 'paste'
    color = '#4D7C0F'
    description = `Nutritional pastes combine the advantage of having
        many artificial flavors while maintaining a high nutritional value.`
}
