import Production from '../Production'

export default class EleriumAccelerator extends Production {
    resourceName = 'elerium'
    color = '#101555'
    description = `The Elerium (or e115) is obtained by bombarding calcium.
        Very reactive, it can provide an important quantity of energy.`
}
