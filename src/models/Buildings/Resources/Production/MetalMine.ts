import Production from '../Production'

export default class MetalMine extends Production {
    resourceName = 'metal'
    color = '#9ca3af'
    description = `The metal mines are cold and dark. You'll find everything you need
        to strengthen your units and progress through the game with peace of mind.`
}
