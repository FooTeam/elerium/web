import type { Writable } from 'svelte/store'
import { writable } from 'svelte/store'
import type Production from '$models/Buildings/Resources/Production'
import type Storage from '$models/Buildings/Resources/Storage'
import type District from '$models/District'
import DistrictField from '$models/DistrictField'
import { get } from '$lib/fetch'
import type {
    Building as BuildingApiType,
    Starshipyard as StarshipyardApiType
} from '$utils/API/Building.type'
import type Starshipyard from '$models/Buildings/Starshipyard'

export type neededResources = {
    metal?: number
    graphite?: number
    elerium?: number
    paste?: number
}

export default abstract class Building extends DistrictField {
    level: number
    apiHydrated: Writable<boolean>
    private up: {
        neededMetal: number
        neededGraphite: number
    }

    constructor(x: number, y: number, district: District, level?: number) {
        super(x, y, district)
        this.level = level || 0
        this.apiHydrated = writable(false)
    }

    async hydrate(): Promise<void> {
        const { res } = (await get(`api/districts/${this.district.id}/building`, {
            value: this.constructor.name,
            x: this.x,
            y: this.y
        })) as {
            res: BuildingApiType
        }
        this.setApiData(res)
        this.apiHydrated.set(true)
    }

    public getNeededResources(): neededResources {
        return {
            metal: this.up.neededMetal,
            graphite: this.up.neededGraphite
        }
    }

    isProduction(): this is Production {
        return 'production' in this
    }

    isStorage(): this is Storage {
        return 'storage' in this
    }

    isStarshipyard(): this is Starshipyard {
        return 'unitStorage' in this
    }

    upgrade(): void {
        this.apiHydrated.set(false)
        this.level++
        this.hydrate()
    }

    protected setApiData(res: BuildingApiType): void {
        this.up = res.up
        this.level = res.level

        if (this.isStarshipyard()) {
            this.setUnitStorage(res as StarshipyardApiType)
        }
    }
}
