import type District from '$models/District'
import type Building from '$models/Building'
import getHumanName from '$utils/getHumanName'
import { post } from '$lib/fetch'

export enum DistrictFieldGradientType {
    linGrad,
    radGrad
}

type DistrictFieldColorCode = string

export type DistrictFieldGradientColorStop = {
    offset: number
    color: string
}

type DistrictFieldGradient = {
    type: DistrictFieldGradientType
    colors: Array<DistrictFieldGradientColorStop>
}

export type DistrictFieldColor = DistrictFieldColorCode | DistrictFieldGradient

export default abstract class DistrictField {
    abstract description: string
    abstract color: DistrictFieldColor

    protected constructor(
        public x: number,
        public y: number,
        protected readonly district: District
    ) {}

    getShortName(): string {
        return (
            this.constructor.name
                .split(/(?=[A-Z])/)
                .reduce((prev, curr) => prev + curr.charAt(0), '') || ''
        )
    }

    name(): string {
        return getHumanName(this.constructor.name)
    }

    isBuilding(): this is Building {
        return Object.prototype.hasOwnProperty.call(this, 'level')
    }

    demolish(): Promise<unknown> {
        const req = post(`api/districts/${this.district.id}/demolish`, {
            x: this.x,
            y: this.y
        })

        // Update the resources when demolishing
        if (this.isBuilding()) {
            if (this.isProduction()) {
                const { resources, store } = this.district.getResources()
                resources[this.resourceName].production -= this.production.total
                store.set(resources)
            } else if (this.isStorage()) {
                const { resources, store } = this.district.getResources()
                resources[this.resourceName].max -= this.storage.total
                store.set(resources)
            }
        }

        this.district.grid.update(grid => {
            grid[this.y][this.x] = null
            return grid
        })

        return req
    }
}
