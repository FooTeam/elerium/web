import { Writable, writable } from 'svelte/store'
import type BaseError from '$lib/errors/Errors/BaseError'

export { BuildingMustBeResearchedError } from './Errors/BuildingMustBeResearchedError'
export { DistrictLevelTooLowError } from './Errors/DistrictLevelTooLowError'
export { NotEnoughResourcesError } from './Errors/NotEnoughResourcesError'
export { NeededScienceError } from './Errors/NeededScienceError'
export { NeededEleriumError } from './Errors/NeededEleriumError'

export interface IdentifiedError {
    id: number
    name: string
    message: string
}

export const errors: Writable<IdentifiedError[]> = writable([])

export const pushError = (err: BaseError): void => {
    const uid = Date.now() + Math.floor(Math.random() * 1000)
    const { name, message } = err
    errors.update(queue => [{ id: uid, name, message }, ...queue])
}
