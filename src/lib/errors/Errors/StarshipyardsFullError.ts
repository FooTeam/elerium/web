import BaseError from './BaseError'

export default class StarshipyardsFullError extends BaseError {
    constructor() {
        super(`Starshipyards are full.`, 'Improve or build more starshipyards.')
    }
}
