import BaseError from './BaseError'

export class UnauthorizedError extends BaseError {
    constructor() {
        super('You have been disconnected.', 'Please reconnect to access this page.')
    }
}
