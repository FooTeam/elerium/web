import BaseError from './BaseError'

export class DistrictLevelTooLowError extends BaseError {
    constructor(building: string) {
        super(`Cannot upgrade ${building}.`, 'Upgrade your District Council before.')
    }
}
