import BaseError from './BaseError'

export class BuildingMustBeResearchedError extends BaseError {
    constructor(building: string) {
        super(`${building} must be researched.`)
    }
}
