import BaseError from './BaseError'

export class NeededScienceError extends BaseError {
    constructor(research: string, message: string) {
        super(`Missing Science to unlock the research ${research}.`, message)
    }
}
