import BaseError from './BaseError'

type ResourcePayload = {
    name: string
    have: number
    need: number
}

export class NotEnoughResourcesError extends BaseError {
    constructor(building: string, ...resources: ResourcePayload[]) {
        super(
            `Not enough resources to build: ${building}.`,
            resources.reduce((carry: string, resource: ResourcePayload) => {
                return `${carry}[${resource.name}] have: ${resource.have}; need: ${resource.need}<br />`
            }, '')
        )
    }
}
