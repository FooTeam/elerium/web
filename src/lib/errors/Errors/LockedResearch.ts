import BaseError from './BaseError'

export class LockedResearchError extends BaseError {
    constructor(research: string) {
        super(`${research} must be researched.`)
    }
}
