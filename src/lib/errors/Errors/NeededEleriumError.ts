import BaseError from './BaseError'

export class NeededEleriumError extends BaseError {
    constructor(message: string) {
        super(`Missing elerium.`, message)
    }
}
