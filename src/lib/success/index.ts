import { Writable, writable } from 'svelte/store'

export const success: Writable<{ id: number; message: string }[]> = writable([])

export const pushSuccess = (message: string): void => {
    const uid = Date.now() + Math.floor(Math.random() * 1000)
    success.update(queue => [{ id: uid, message }, ...queue])
}
