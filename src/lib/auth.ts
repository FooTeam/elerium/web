import { goto } from '$app/navigation'
import { post } from './fetch'
import user from './user'
import type { User } from './user'

export const login = async (username: string, password: string): Promise<unknown> => {
    const { req, res } = await post('login', { username, password })
    if (req.ok) {
        user.set(res as User)
        return goto('/game')
    }
}

export const register = async (
    username: string,
    password: string,
    email: string
): Promise<unknown> => {
    const { req, res } = await post('register', { email, username, password })
    if (req.ok) {
        return await login(username, password)
    }
    return res.errors
}
