import { writable } from 'svelte/store'
import type { Readable } from 'svelte/store'

export type MercureStore<T> = Readable<T> & { close: () => void }

export default (topic: string): MercureStore<unknown> => {
    const { subscribe, update } = writable([])

    const url = new URL(import.meta.env.VITE_CLIENT_URL + '.well-known/mercure')
    url.searchParams.append('topic', import.meta.env.VITE_CLIENT_URL + topic)
    const eventSource = new EventSource(url.href, { withCredentials: true })

    eventSource.onmessage = ({ data }) => {
        update(messages => messages.concat(JSON.parse(data)))
    }

    return {
        subscribe,
        close: () => eventSource.close()
    }
}
