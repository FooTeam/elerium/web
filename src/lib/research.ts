import Research from '$models/Research'
import { onMount } from 'svelte'
import type { Writable } from 'svelte/store'
import { writable } from 'svelte/store'
import type { ResearchResource } from '$utils/API/Research.type'
import { get } from './fetch'

export const researchTree: Writable<Research[][]> = writable([], set => {
    onMount(async () => {
        const { res } = (await get('api/researches')) as {
            res: ResearchResource
        }
        const research = new Research(res)
        const max = maxDepth(0, research)
        const result = []
        for (let index = 0; index < max; index++) {
            result.push(getLevel(index, research))
        }
        set(result)
    })
})

const maxDepth = (depth, tree): number => {
    let max = depth
    tree.unlocks.forEach(unlock => {
        const tmpDepth = maxDepth(++depth, unlock)
        if (tmpDepth > max) {
            max = tmpDepth
        }
    })
    return max
}

export const getLevel = (requestedDepth: number, first: Research): Research[] => {
    if (first.unlocks.length === 0) return []
    if (requestedDepth === 0) return [first]
    // init
    let depth = 0
    const queue: Research[] = []
    let children: Research[] = []
    queue.push(first)

    while (queue.length > 0) {
        const current: Research = queue.shift()
        current.unlocks.forEach(unlock => {
            children.push(unlock)
        })
        if (queue.length === 0) {
            children.forEach(child => queue.push(child))
            children = []
            depth++
        }
        if (depth === requestedDepth) {
            return queue
        }
    }
}
