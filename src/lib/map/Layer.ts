import Tile from '$lib/map/Tile'

export class Layers extends Map<string, Layer> {}

export class Layer {
    constructor(readonly context: CanvasRenderingContext2D, private tiles: Tile[] = []) {}

    get ctx(): CanvasRenderingContext2D {
        return this.context
    }

    setTiles(tiles: Tile[]): void {
        this.tiles = tiles
    }

    getTiles(): Tile[] {
        return this.tiles
    }

    getTile(tile: Tile): Tile | undefined {
        return this.tiles.find(t => Tile.distance(t, tile) === 0)
    }

    hasTile(tile: Tile): boolean {
        return Boolean(this.getTile(tile))
    }
}
