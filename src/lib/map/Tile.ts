import type Layout from '$lib/map/Layout'
import type DistrictField from '$models/DistrictField'
import type { TilePaint } from './TilePainter'

type Cuboid = { q: number; r: number; s: number }
type Axial = { q: number; r: number }
type Coordinates = Cuboid | Axial

export type TileDrawingProperties = {
    color: TilePaint
    name: string
    level?: number
}

export default class Tile {
    static DEFAULT_FILL_COLOR = '#F5F5F5'
    static DEFAULT_STROKE_COLOR = '#D4D4D4'
    readonly q: number // x = q + offset
    readonly r: number // y = r
    readonly s: number
    readonly offset: number
    public districtField: DistrictField

    constructor(coordinates: Coordinates) {
        const { q, r, s } = Tile.axialToCuboid(coordinates)
        this.q = q
        this.r = r
        this.s = s
        this.offset = Math.floor(r / 2)
    }

    /**
     * Turns fractional tile coordinates into the nearest integer ones
     */
    static round(tile: Tile): Tile {
        let q: number = Math.round(tile.q)
        let r: number = Math.round(tile.r)
        let s: number = Math.round(tile.s)

        const qDiff: number = Math.abs(q - tile.q)
        const rDiff: number = Math.abs(r - tile.r)
        const sDiff: number = Math.abs(s - tile.s)

        if (qDiff > rDiff && qDiff > sDiff) {
            q = -r - s
        } else {
            if (rDiff > sDiff) {
                r = -q - s
            } else {
                s = -q - r
            }
        }

        return new Tile({ q, r, s })
    }

    /**
     * Subtracts the coordinates of two given Tile
     */
    public static subtract(a: Tile, b: Tile): Tile {
        return new Tile({
            q: a.q - b.q,
            r: a.r - b.r,
            s: a.s - b.s
        })
    }

    /**
     * Calculates the length of the line between two Tile in Tile
     */
    public static len(tile: Tile): number {
        return (Math.abs(tile.q) + Math.abs(tile.r) + Math.abs(tile.s)) / 2
    }

    /**
     * Calculates the distance between two Tile in a Tile grid in Tile
     */
    public static distance(a: Tile, b: Tile): number {
        return Tile.len(Tile.subtract(a, b))
    }

    setDistrictField(districtField: DistrictField): Tile {
        this.districtField = districtField

        return this
    }

    /**
     * Add the coordinates of an Tile to the current one
     */
    add(toAdd: Tile): Tile {
        const { q, r, s } = this

        return new Tile({
            q: q + toAdd.q,
            r: r + toAdd.r,
            s: s + toAdd.s
        })
    }

    /**
     * Draw the Tile shape and its content
     */
    draw(ctx: CanvasRenderingContext2D, layout: Layout, props: TileDrawingProperties): void {
        this.drawShape(ctx, layout, props.color)
        this.drawName(ctx, layout, props.name)
        if (props.level) this.drawLevel(ctx, layout, props.level)
    }

    /**
     * Draw a not centered level in the Tile
     */
    drawLevel(ctx: CanvasRenderingContext2D, layout: Layout, level: number): void {
        Tile.centerText(ctx)
        ctx.font = '12px sans-serif'
        ctx.fillText(String(level), layout.tileToPoint(this).x, layout.tileToPoint(this).y + 18)
    }

    /**
     * Convert Axial to Cuboid coordinates
     */
    private static axialToCuboid(coordinates: Axial): Cuboid {
        const { q, r } = coordinates

        return { q, r, s: -q - r }
    }

    /**
     * Draw the Tile shape
     */
    private drawShape(ctx: CanvasRenderingContext2D, layout: Layout, color: TilePaint) {
        ctx.fillStyle = color
        ctx.strokeStyle = this.districtField?.isBuilding() ? '#525252' : Tile.DEFAULT_STROKE_COLOR
        ctx.lineWidth = this.districtField?.isBuilding() ? 8 : 1
        ctx.lineCap = 'square'

        ctx.beginPath()
        for (const point of layout.tileCorners(this, this.districtField?.isBuilding() ? 4 : 0.5)) {
            ctx.lineTo(point.x, point.y)
        }

        ctx.stroke()
        ctx.fill()
    }

    /**
     * Center text in the Tile
     */
    private static centerText(ctx: CanvasRenderingContext2D) {
        ctx.fillStyle = '#fff'
        ctx.textAlign = 'center'
        ctx.textBaseline = 'middle'
        ctx.font = '16px sans-serif'
    }

    /**
     * Draw a centered shortname in the Tile
     */
    private drawName(ctx: CanvasRenderingContext2D, layout: Layout, name: string) {
        Tile.centerText(ctx)
        ctx.fillText(name, layout.tileToPoint(this).x, layout.tileToPoint(this).y)
    }
}
