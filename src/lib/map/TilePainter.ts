import {
    DistrictFieldColor,
    DistrictFieldGradientColorStop,
    DistrictFieldGradientType
} from '$models/DistrictField'
import type Layout from './Layout'
import Orientation from './Orientation'
import type Point from './Point'

export type TilePaint = string | CanvasGradient

export default class TilePainter {
    constructor(private layout: Layout) {}

    /**
     * Add color stops to gradients following the DistrictField configuration
     */
    private static addColorStops(
        colorStops: Array<DistrictFieldGradientColorStop>,
        gradient: CanvasGradient
    ): CanvasGradient {
        for (const { offset, color } of colorStops) {
            gradient.addColorStop(offset, color)
        }

        return gradient
    }

    /**
     * Create the color from the DistrictField configuration
     */
    createColor(
        context: CanvasRenderingContext2D,
        color: DistrictFieldColor,
        center: Point
    ): TilePaint {
        if (!color) return

        if (typeof color === 'string') {
            return color
        }

        const halfWidth = Orientation.pointy.forward[1] * this.layout.size.x
        if (color.type === DistrictFieldGradientType.linGrad) {
            return this.buildLinearGradient(context, color.colors, center, halfWidth)
        }

        if (color.type === DistrictFieldGradientType.radGrad) {
            return this.buildRadialGradient(context, color.colors, center, halfWidth)
        }
    }

    /**
     * Build a linear gradient from the Tile top left to the Tile bottom right
     */
    private buildLinearGradient(
        context: CanvasRenderingContext2D,
        colorStops: Array<DistrictFieldGradientColorStop>,
        center: Point,
        halfWidth: number
    ): CanvasGradient {
        return TilePainter.addColorStops(
            colorStops,
            context.createLinearGradient(
                center.x - halfWidth / 2,
                center.y - this.layout.size.x,
                center.x + halfWidth / 2,
                center.y + this.layout.size.x
            )
        )
    }

    /**
     * Build a radial gradient from the Tile center
     */
    private buildRadialGradient(
        context: CanvasRenderingContext2D,
        colorStops: Array<DistrictFieldGradientColorStop>,
        center: Point,
        halfWidth: number
    ): CanvasGradient {
        return TilePainter.addColorStops(
            colorStops,
            context.createRadialGradient(
                center.x,
                center.y,
                this.layout.size.x / 6,
                center.x,
                center.y,
                halfWidth
            )
        )
    }
}
