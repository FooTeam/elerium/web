import { get } from 'svelte/store'
import Tile, { TileDrawingProperties } from '$lib/map/Tile'
import Layout from '$lib/map/Layout'
import Point from '$lib/map/Point'
import Orientation from '$lib/map/Orientation'
import { Layer, Layers } from '$lib/map/Layer'
import type District from '$models/District'
import TilePainter from './TilePainter'

export default class Map {
    static ERROR_MARGIN_IN_PIXELS = 2

    stage: HTMLElement
    canvasWidth: number
    canvasHeight: number

    columns = 15
    rows = 11

    hovered: Tile
    selected: Tile

    layout: Layout
    tilePainter: TilePainter

    private layers: Layers = new Layers()

    constructor(private district: District) {}

    getDistrict(): District {
        return this.district
    }

    setStage(stage: HTMLElement): Map {
        this.stage = stage
        this.canvasWidth = stage.clientWidth
        stage.querySelectorAll('canvas').forEach(canvas => {
            this.addLayer(canvas)
        })

        return this
    }

    addLayer(layer: HTMLCanvasElement): void {
        const name: string = layer.id.replace('layer__', '')
        const context: CanvasRenderingContext2D = layer.getContext('2d')

        this.layers.set(name, new Layer(context))
    }

    build(): Map {
        this.drawBackground()
        this.resize()

        return this
    }

    /**
     * Draw each Tile of the map on the canvas context
     */
    draw(): void {
        const mapLayer = this.layers.get('map')
        mapLayer.ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight)
        mapLayer.ctx.save()

        const grid = get(this.district.grid)

        const fields: Tile[] = []
        for (let r = 0; r < this.rows; r++) {
            const offset = Math.floor(r / 2)
            for (let q = -offset; q < this.columns - offset; q++) {
                const field = grid?.[r]?.[q + offset]
                const tile = new Tile({ q, r }).setDistrictField(field)
                fields.push(tile)

                const props: TileDrawingProperties = {
                    name: '',
                    level: null,
                    color: Tile.DEFAULT_FILL_COLOR
                }
                if (field) {
                    if (field.isBuilding()) props.name = field.getShortName()
                    if (field.isBuilding()) props.level = field.level
                    props.color = this.tilePainter.createColor(
                        mapLayer.ctx,
                        field.color,
                        this.layout.tileToPoint(tile)
                    )
                }
                tile.draw(mapLayer.ctx, this.layout, props)
            }
        }
        mapLayer.setTiles(fields)

        mapLayer.ctx.restore()
    }

    resize(): Map {
        this.canvasWidth = this.stage.clientWidth
        this.setLayout()

        this.stage.style.height = this.canvasHeight + 'px'
        for (const layer of this.layers.values()) {
            layer.ctx.canvas.width = this.canvasWidth
            layer.ctx.canvas.height = this.canvasHeight
        }

        return this
    }

    select(): void {
        const context = this.layers.get('interaction').ctx
        this.clearTile(context, this.selected)

        this.selected = this.hovered

        context.save()

        this.selected.draw(context, this.layout, {
            name: this.selected.districtField?.getShortName() || '',
            color: this.tilePainter.createColor(
                context,
                'rgba(0, 0, 0, 0.3)',
                this.layout.tileToPoint(this.selected)
            )
        })

        context.restore()
    }

    /**
     * Mask the hovered Tile with a semi transparent black Tile
     */
    hover(event: MouseEvent): void {
        const context = this.layers.get('hover').ctx
        const hoveredTile = this.layout.pointToTile(Layout.eventToPoint(event))

        let tile: Tile
        if ((tile = this.layers.get('map').getTile(hoveredTile))) {
            this.clearTile(context, this.hovered)

            context.save()

            hoveredTile.draw(context, this.layout, {
                name: tile.districtField?.getShortName() || '',
                color: this.tilePainter.createColor(
                    context,
                    'rgba(0, 0, 0, 0.3)',
                    this.layout.tileToPoint(tile)
                )
            })

            this.hovered = tile

            context.restore()
        }
    }

    blur(): void {
        this.deselect()
        this.leave()
    }

    /**
     * Can be called on Svelte onDestroy lifecycle event
     */
    destroy(): null {
        return null
    }

    private setLayout() {
        const edgeSize = this.calculateEdgeSize()
        const size = new Point(edgeSize, edgeSize)
        const origin = new Point(Orientation.pointy.forward[1] * size.x, size.y)

        this.layout = new Layout(Orientation.pointy, size, origin)
        this.calculateCanvasHeight()
        this.tilePainter = new TilePainter(this.layout)
    }

    /**
     * Calculate the distance between the center and any corner of on Tile depending on the grid to create
     */
    private calculateEdgeSize() {
        return (
            this.canvasWidth /
            (this.columns * Orientation.pointy.forward[0] + Orientation.pointy.forward[1])
        )
    }

    /**
     * Calculate the Canvas height depending on the calculated Tile size
     */
    private calculateCanvasHeight() {
        const edgeSize = this.layout.size.x
        this.canvasHeight = this.rows * ((2 * edgeSize * 3) / 4) + edgeSize / 2
    }

    private drawBackground() {
        const context = this.layers.get('background').ctx
        context.save()
        context.fillStyle = '#fff'
        context.fill()
        context.restore()
    }

    private deselect() {
        this.clearTile(this.layers.get('interaction').ctx, this.selected)
        this.selected = undefined
    }

    private leave() {
        this.clearTile(this.layers.get('hover').ctx, this.hovered)
        this.hovered = undefined
    }

    private clearTile(context: CanvasRenderingContext2D, tile: Tile) {
        if (tile instanceof Tile) {
            const edgeSize = this.layout.size.x
            const tileWidth = Orientation.pointy.forward[0] * edgeSize
            const tileHalfWidth = Orientation.pointy.forward[1] * edgeSize
            const tileHeight = 2 * edgeSize
            const tileCenter: Point = this.layout.tileToPoint(tile)

            context.clearRect(
                tileCenter.x - tileHalfWidth - Map.ERROR_MARGIN_IN_PIXELS,
                tileCenter.y - edgeSize - Map.ERROR_MARGIN_IN_PIXELS,
                // Since we substract ERROR_MARGIN to the x and y positions,
                // We have to add the double of it to the width and height
                tileWidth + 2 * Map.ERROR_MARGIN_IN_PIXELS,
                tileHeight + 2 * Map.ERROR_MARGIN_IN_PIXELS
            )
        }
    }
}
