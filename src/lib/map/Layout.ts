import type Orientation from '$lib/map/Orientation'
import Tile from '$lib/map/Tile'
import Point from '$lib/map/Point'

export default class Layout {
    constructor(public orientation: Orientation, public size: Point, public origin: Point) {}

    /**
     * Convert an event to a Point
     */
    static eventToPoint(event: MouseEvent): Point {
        return new Point(event.offsetX, event.offsetY)
    }

    /**
     * Convert an Tile coordinates to pixel coordinates
     * The returned Point corresponds to the Tile center
     */
    tileToPoint(tile: Tile): Point {
        const f = this.orientation.forward
        const { q, r } = tile
        const x = (f[0] * q + f[1] * r) * this.size.x
        const y = (f[2] * q + f[3] * r) * this.size.y

        return new Point(x + this.origin.x, y + this.origin.y)
    }

    /**
     * Convert pixel coordinates to Tile grid coordinates
     * Returns the corresponding Tile
     */
    pointToTile(point: Point): Tile {
        const b = this.orientation.inverse
        const p = new Point(
            (point.x - this.origin.x) / this.size.x,
            (point.y - this.origin.y) / this.size.y
        )

        const q = b[0] * p.x + b[1] * p.y
        const r = b[2] * p.x + b[3] * p.y

        return Tile.round(new Tile({ q, r }))
    }

    /**
     * Get all six Tile corners as Points
     */
    tileCorners(tile: Tile, offset: number): Array<Point> {
        const corners: Array<Point> = []
        const center: Point = this.tileToPoint(tile)

        for (let theta = 0; theta <= Math.PI * 2; theta += Math.PI / 3) {
            corners.push(
                new Point(
                    center.x + (this.size.x - offset) * Math.sin(theta),
                    center.y + (this.size.y - offset) * Math.cos(theta)
                )
            )
        }

        return corners
    }
}
