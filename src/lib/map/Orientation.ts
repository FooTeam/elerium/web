type Matrix = [number, number, number, number]

export default class Orientation {
    /**
     * Pointy matrices to convert a Tile to pixel and vice versa
     * https://www.redblobgames.com/grids/hexagons/implementation.html#layout
     */
    static pointy = new Orientation(
        [Math.sqrt(3.0), Math.sqrt(3.0) / 2.0, 0.0, 3.0 / 2.0],
        [Math.sqrt(3.0) / 3.0, -1.0 / 3.0, 0.0, 2.0 / 3.0]
    )

    private constructor(public forward: Matrix, public inverse: Matrix) {}
}
