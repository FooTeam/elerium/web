import { localWritable } from './localStorage'
import type { Writable } from 'svelte/store'

export type User = {
    id: number
    username: string
    email: string
}

export default localWritable('user', undefined) as Writable<User>
