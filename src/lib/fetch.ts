import { get as storeGet, writable } from 'svelte/store'
import { getXsrfToken } from './xsrf'
import user from './user'
import { pushError } from './errors'
import { UnauthorizedError } from './errors/Errors/UnauthorizedError'

export const awaitingAPI = writable(false)

type FetchPromise = Promise<{ res: any; req: Response }>

export const get = (input: string, data = {}): FetchPromise =>
    req(input + '?' + objectToQueryString(data))

export const post = (input: string, body: Record<string, unknown>): FetchPromise =>
    req(input, 'POST', { body: JSON.stringify(body) })

export const patch = (input: string, body: Record<string, unknown>): FetchPromise =>
    req(input, 'PATCH', { body: JSON.stringify(body) })

export const del = (input: string): FetchPromise => req(input, 'DELETE')

/**
 * Build the request headers object
 * @param {string} method request HTTP method
 */
const getHeaders = async (method: string): Promise<{ 'Content-Type': string; Accept: string }> => {
    const headers = {
        'Content-Type': 'application/json',
        Accept: 'application/json'
    }

    if (method !== 'GET') {
        headers['X-XSRF-TOKEN'] = await getXsrfToken()
    }

    return headers
}

const req = async (input: string, method = 'GET', init = {}): FetchPromise => {
    awaitingAPI.set(true)
    return fetch(`${import.meta.env.VITE_API_URL}/${sanitizeInput(input)}`, {
        method,
        credentials: 'include',
        headers: await getHeaders(method),
        ...init
    }).then(async req => {
        awaitingAPI.set(false)

        if (req.status === 401) {
            if (storeGet(user) !== undefined) {
                pushError(new UnauthorizedError())
            }
            user.set(undefined)
        }

        return req.ok
            ? { req, res: req.status !== 204 ? await req.json() : null }
            : Promise.reject(await req.json())
    })
}

/**
 * Trim the first "/" of the input if exists
 */
const sanitizeInput = (input: string): string =>
    input.charAt(0) === '/' ? input.substring(1) : input

const objectToQueryString = (object: Record<string, string>): string =>
    Object.keys(object)
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(object[key])}`)
        .join('&')
