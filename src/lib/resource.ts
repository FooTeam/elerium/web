export type Resources = {
    elerium: Resource
    graphite: Resource
    metal: Resource
    paste: Resource
}

type Resource = {
    value: number
    production: number
    max: number
}
