import type { Writable } from 'svelte/store'
import { writable } from 'svelte/store'
import { onMount } from 'svelte'
import { get } from '$lib/fetch'
import type { DistrictResourceCollection } from '$utils/API/District.type'
import District from '$models/District'

export const districts: Writable<District[]> = writable([], set => {
    onMount(async () => {
        const { res } = (await get('api/districts')) as {
            res: DistrictResourceCollection
        }

        const districts: District[] = []

        for (const district of res.data) {
            districts[district.id] = new District(district)
        }

        set(districts)
    })

    return () => null
})
