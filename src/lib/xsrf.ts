export const xsrfTokenRefresh = async (): Promise<void> => {
    await fetch(import.meta.env.VITE_API_URL + '/sanctum/csrf-cookie', {
        credentials: 'include'
    })
}

export const getXsrfToken = async (): Promise<string> => {
    await xsrfTokenRefresh()

    for (const c of document.cookie.split(';')) {
        const cookie = c.split('=')
        if (cookie[0] === 'XSRF-TOKEN') {
            return decodeURIComponent(cookie[1])
        }
    }
}
