const sans = require('tailwindcss/defaultTheme').fontFamily.sans
const trueGray = require('tailwindcss/colors').trueGray
const orange = require('tailwindcss/colors').orange
const teal = require('tailwindcss/colors').teal
const fuchsia = require('tailwindcss/colors').fuchsia
const purple = require('tailwindcss/colors').purple
const indigo = require('tailwindcss/colors').indigo

module.exports = {
    mode: 'jit',
    purge: ['./src/**/*.{svelte,html}'],
    darkMode: 'media',
    theme: {
        container: {
            padding: {
                default: '1rem',
                sm: '2rem',
                lg: '4rem',
                xl: '5rem'
            }
        },
        extend: {
            boxShadow: {
                error: '0 0 0 4px rgba(229, 62, 62, 0.5)'
            },
            colors: {
                tile: {
                    default: '#ddd',
                    blue: '#0ea5e9',
                    red: 'red',
                    yellow: 'yellow',
                    orange: 'orange',
                    violet: 'violet'
                    //  green: bg-green-400
                },
                orange,
                teal,
                midnight: {
                    100: '#7074A1',
                    200: '#626698',
                    300: '#52578e',
                    400: '#414683',
                    500: '#2e3377',
                    600: '#191f69',
                    700: '#02095a',
                    800: '#020644',
                    900: '#020538'
                },
                trueGray,
                fuchsia,
                indigo,
                purple
            },
            fontFamily: {
                display: ['Bebas Neue'],
                poppins: ['Poppins', ...sans]
            },
            fontSize: {
                '8xl': '6rem'
            },
            inset: {
                4: '1rem'
            },
            opacity: {
                90: '0.9'
            },
            zIndex: {
                '-10': '-10'
            }
        }
    },
    variants: {},
    plugins: []
}
